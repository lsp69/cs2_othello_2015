Othello Project Summary.

I think Jordan and I worked well together on this project. We both did out share of coding 
as well as planning out and strategizing ways to win. Jordan was able to fix some critical
bugs in the code that were preventing it from running correctly. 


AI Strategy: The first improvement we made this week was to implement alpha-beta 
pruning. In addition, we added many mechanisms to compute heuristic score. We changed
back and forth between models, and eventually settled on model that took into account
piece placement, and mobility - number of availible moves. And in the endgame (less than 
roughly 10 pieces left) we did a brute force calculation to calculate the best move. 

If I could I wish we could have implemented an opening book strategy. I would have 
ran our heuristic, but with much bigger depth, and stored the results in a table 
mapping a board state to a move. Unfortunately, we did not get around to this. Overall,
we enjoyed this project and both thought we learned a lot from it. 