#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <limits>

using namespace std;

class Player {
	
public:	

	struct boardstruct
	{
		Board * myBoard;
		vector<boardstruct *> adjBoards;
		boardstruct(Board * inputBoard)
		{
			this->myBoard = inputBoard;
		}
		
		
	};


    Player(Side side);
    ~Player();
    
    
    void makeTree(int depth, boardstruct * parent);
    int calcScore_early(Board *b);
    int calcScore_mid(Board * b);
    int calcScore(Board * b);
    int calcScore_late(Board *b);
    vector<Move*> get_possible_moves(Board * b, Side my_color);
    Move *doMove(Move *opponentsMove, int msLeft);
    int alphaBeta(boardstruct * origin, int depth, int a, int b, bool parity);

    
    
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    int DEPTH;
    Side color;
    Side oColor;
    Board * board;

};




#endif
