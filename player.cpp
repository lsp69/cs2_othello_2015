#include "player.h"
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h> 

# define rogue_city 420
# define INITIAL_DEPTH 3


/*
 * Brotha Ethan has made a change..
 * Figuring out git is harder than the AI #deepkwok
 */

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    color = side;
    DEPTH = INITIAL_DEPTH;
    if(color == BLACK){
		oColor = WHITE;
	}
	else{
		oColor = BLACK;
	}
	    
    board = new Board();
    

    
}

/*
 * Destructor for the player.
 */
Player::~Player() {
delete board;
}

// Method that creates the tree. You have to make a head to start.
// depth: the depth of the tree
// parent: the pointer that points to the current boardstruct: we are trying to fill in its children.
// is_color: true if color == our color, false if color == opponents color
void Player::makeTree(int max_depth, boardstruct * parent){
	
	Side current_color = color;
	
	vector<Move*> possible_moves = get_possible_moves(parent->myBoard, current_color);
    vector<Player::boardstruct*> possible_boards; 
    
    	for(unsigned int k = 0; k < possible_moves.size(); k++)
    	{
		    Board * tboard = new Board();
		    tboard = parent->myBoard->copy();
		    tboard->doMove(possible_moves[k], current_color);
		    boardstruct * to_copy = new boardstruct(tboard);
		    parent->adjBoards.push_back(to_copy);
		    possible_boards.push_back(to_copy);

		}

		//depth 1 done
		
	for(int z = 1; z < max_depth; z++) //go from depth 2 to desired depth
	{
		if(DEPTH == 100)
		{
			fprintf(stderr, "\nBruteforce in progress...");
	        fprintf(stderr, "\nBruteforce penetration depth %i completed\n", z);
        }
        
		if(current_color == BLACK)
		{
			current_color = WHITE;
		}
		else
		{
			current_color = BLACK;
		}
		
	    //Used to be its own function
		vector<boardstruct*> replacement;
		for(unsigned int i = 0; i< possible_boards.size(); i++)
		{
			vector<Move *>possible_moves = get_possible_moves(possible_boards[i]->myBoard, current_color);
			for(unsigned int j = 0; j < possible_moves.size(); j++)
			{
				Board * tboard = new Board();
				tboard = possible_boards[i]->myBoard->copy();
				tboard->doMove(possible_moves[j], color);
				boardstruct * to_copy = new boardstruct(tboard);
				possible_boards[i]->adjBoards.push_back(to_copy);
				replacement.push_back(to_copy);
			}
		}
		
	    possible_boards = replacement;
	    //----
	
	
		if(possible_boards.size() == 0)
		{
		    return;
	    }

    }
}




vector<Move*> Player::get_possible_moves(Board * b, Side my_color)
{
	vector <Move *> output;
     for(int i = 0; i < 8; i++)
	 {
		 for(int j = 0; j < 8; j++)
		 {
			 Move * temp = new Move(i, j);
			 if(b->checkMove(temp, my_color))
			 {
				 output.push_back(temp);
			 }
			 else
			 {
				 delete temp;
			 }
		 }
	 }
	 return output;
}

//Decide which type of score metrics to use
int Player::calcScore(Board * b)
{
	int num_peices = b->count(color) + b->count(oColor);
	
    if(num_peices < 15)
    {
		return calcScore_early(b);
	}

    else if(num_peices > 58) // bruteforce if 7 spaces or less left
    {
		return calcScore_late(b);
	}
    else
    {
		return calcScore_mid(b);
	}
}

//Account for raw chip count, corners, mobility.
int Player::calcScore_mid(Board * b)
{

	int weight = 0;
	
	// mobility
	int our_possible_moves = get_possible_moves(b, color).size();
    int opp_possible_moves = get_possible_moves(b, oColor).size();
    int possible_move_weight = our_possible_moves - opp_possible_moves;	
	weight = weight + 1 * possible_move_weight;
	
	
	// Heuristic weight table
	
	// Corner
	if(b->get(color,0,0) || b->get(color,7,0) || b->get(color,0,7) || b->get(color,7,7)){
		weight += 4;
	}
	
	// Next to corner
	if(b->get(color,1,0) || b->get(color,0,1)){
		if(!(b->get(color,0,0))){
			weight -= 7;
		}
	}
	if(b->get(color,6,0) || b->get(color,7,1)){
		if(!(b->get(color,7,0))){
			weight -= 7;
		}
	}
	if(b->get(color,0,6) || b->get(color,1,7)){
		if(!(b->get(color,0,7))){
			weight -= 7;
		}
	}
	if(b->get(color,6,7) || b->get(color,7,6)){
		if(!(b->get(color,7,7))){
			weight -= 7;
		}
	}
	
	// Middle Pieces
	for(int i = 2; i <= 5; i++){
		if(b->get(color,0,i) || b->get(color,7,i)){
			weight += 2;
		}
		if(b->get(color,1,i) || b->get(color,6,i)){
			weight--;
		}
		
		if(b->get(color,i,0) || b->get(color,i,7)){
			weight += 2;
		}
		if(b->get(color,i,1) || b->get(color,i,6)){
			weight--;
		}
	}
	if(b->get(color,2,2) || b->get(color,2,5) || b->get(color,5, 2) || b->get(color,5,5) || b->get(color,3,3) || b->get(color,3,4) || b->get(color,4,3) || b->get(color,4,4)){
	    weight++;
	}
	
	
	
	return weight;
	
	/*
    int weight = (300 * ((b->count(color) - b->count(oColor)))  );
=======
    int weight = (100 * ((b->count(color) - b->count(oColor)))  );
>>>>>>> 300c01a0b37da7b985701e522683b9944e72b5c3
    int our_corners =  (b->get(color,0,0)) + (b->get(color,0,7)) + (b->get(color,7,0)) + (b->get(color,7,7)) ;
    int their_corners = (b->get(oColor,0,0)) + (b->get(oColor,0,7)) + (b->get(oColor,7,0)) + (b->get(oColor,7,7));
    if((our_corners + their_corners) != 0)
    {
        weight += (1000 * ((our_corners - their_corners)) );
		  }            
    
    vector<Move *> our = get_possible_moves(b, color);
    vector<Move *> their = get_possible_moves(b, oColor);
    unsigned int additive_size = our.size() + their.size();
    if(additive_size != 0)
    {
        weight += 200 * (our.size() - their.size());
    }
    
    //DEBUGGING NONSENSICAL WEIGHTS
    //if(weight > 50000)
    //{
	//			fprintf(stderr, "\nour size + their size: %i", additive_size);
	//			fprintf(stderr, "\nw1: %i\n", (300 * ((b->count(color) - b->count(oColor)))  ));
	//	fprintf(stderr, "\nw2: %i\n",  (800 * ((our_corners - their_corners)) ));
//		fprintf(stderr, "\nw2: %i\n", weight += 500 * (our.size() - their.size()) );	

	//	while(true)
	//	{
	//	}
	//}
	*/
}

//maximize the number of opponent chips in relation to you. 
//Tries to gain control of middle 4 squares. 
//Heavily favor corners.
int Player::calcScore_early(Board * b)
{
	int weight = 0;
	
	
		// mobility
	int our_possible_moves = get_possible_moves(b, color).size();
    int opp_possible_moves = get_possible_moves(b, oColor).size();
    int possible_move_weight = our_possible_moves - opp_possible_moves;	
	weight = weight + 2 * possible_move_weight;
	
	for(int i = 1; i <= 6; i++){
		if(b->get(color,1,i)){
			weight-=3;
		}
		if(b->get(color,6,i)){
			weight-=3;
		}
		if(b->get(color,i,1)){
			weight-=3;
		}
		if(b->get(color,i,6)){
			weight-=3;
		}
	}
	for(int j = 2; j <= 5; j++){
		if(b->get(color,2,j)){
			weight += 4;
		}
		if(b->get(color,5,j)){
			weight += 4;
		}
		if(b->get(color,j,2)){
			weight += 4;
		}
		if(b->get(color,j,5)){
			weight += 4;
		}
	}
	for(int k = 3; k <= 4; k++){
		if(b->get(color,3,k)){
			weight += 10;
		}
		if(b->get(color,4,k)){
			weight += 10;
		}
		if(b->get(color,k,3)){
			weight += 10;
		}
		if(b->get(color,k,4)){
			weight += 10;
		}
	}


	// Corner
	if(b->get(color,0,0) || b->get(color,7,0) || b->get(color,0,7) || b->get(color,7,7)){
		weight += 4;
	}
	
	// Next to corner
	if(b->get(color,1,0) || b->get(color,0,1)){
		if(!(b->get(color,0,0))){
			weight -= 7;
		}
	}
	if(b->get(color,6,0) || b->get(color,7,1)){
		if(!(b->get(color,7,0))){
			weight -= 7;
		}
	}
	if(b->get(color,0,6) || b->get(color,1,7)){
		if(!(b->get(color,0,7))){
			weight -= 7;
		}
	}
	if(b->get(color,6,7) || b->get(color,7,6)){
		if(!(b->get(color,7,7))){
			weight -= 7;
		}
	}
	
	// Middle Pieces
	for(int i = 2; i <= 5; i++){
		if(b->get(color,0,i) || b->get(color,7,i)){
			weight += 2;
		}
		if(b->get(color,1,i) || b->get(color,6,i)){
			weight--;
		}
		
		if(b->get(color,i,0) || b->get(color,i,7)){
			weight += 2;
		}
		if(b->get(color,i,1) || b->get(color,i,6)){
			weight--;
		}
	}
	if(b->get(color,2,2) || b->get(color,2,5) || b->get(color,5, 2) || b->get(color,5,5) || b->get(color,3,3) || b->get(color,3,4) || b->get(color,4,3) || b->get(color,4,4)){
	    weight++;
	}
	
	
	//int weight = (100.0 * ((b->count(oColor) - b->count(color))) );
	
   // int our_corners =  (b->get(color,0,0)) + (b->get(color,0,7)) + (b->get(color,7,0)) + (b->get(color,7,7)) ;
   // int their_corners = (b->get(oColor,0,0)) + (b->get(oColor,0,7)) + (b->get(oColor,7,0)) + (b->get(oColor,7,7));
   // if((our_corners + their_corners) != 0)
   // {
   //     weight += (1000 * (our_corners - their_corners) );
   // }
   // int our_middles =  (b->get(color,3,3)) + (b->get(color,3,4)) + (b->get(color,4,3)) + (b->get(color,4,4)) ;
    //int their_middles = 4- our_middles;
   // weight += 100 * (our_middles - their_middles);
    
  
    
    return weight;
}

//maximize the raw number of chips. Should be used concurrently with high depth.
int Player::calcScore_late(Board * b)
{
	DEPTH = 100;
	return b->count(color);

}



int Player::alphaBeta(boardstruct * origin, int depth, int a, int b, bool parity)
{
//	fprintf(stderr, "\nHEYYYYY alphabeta called\n");

	
	if((depth == 0) || (origin->adjBoards.size() == 0))
	{
		return calcScore(origin->myBoard);
	}
	
	else if(parity)
	{
		int v = std::numeric_limits<int>::min();
		for(unsigned int i = 0; i < origin->adjBoards.size(); i++)
		{
			v = std::max(v, alphaBeta(origin->adjBoards[i], depth -1, a, b, false) );
			a = std::max(a, v);
		    if(b <= a)
		    {
				break;
			}

		}

            		//fprintf(stderr, "\na score has been set to: %i\n", origin->score);
		    return v;


	}
	else
	{
		int v = std::numeric_limits<int>::max();
		for(unsigned int i = 0; i < origin->adjBoards.size(); i++)
		{
			v = std::min(v, alphaBeta(origin->adjBoards[i], depth -1, a, b, true) );
			b = std::min(b, v);
			
		    if(b <= a)
		    {
				break;
			}

		}
		    return v;
			
	}
	
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
  
          
     // If opponent makes  move
     if(opponentsMove != NULL){
	     board->doMove(opponentsMove, oColor);
	     //fprintf(stderr, "\nOpponent x move: %i Opponent y move: %i\n", opponentsMove->getX(), opponentsMove->getY());
	 }
	 else
	 {
		fprintf(stderr, "\nOpponent went rogue! Null Move!!!\n");
	 }
	 

	 // If we can't move somewhere stop now
	 if(!board->hasMoves(color)){
		 return NULL;
	 }
	
	 //generate the board tree and populate it by running alphabeta pruning algorithm
	 boardstruct * head = new boardstruct(board);
	 makeTree(DEPTH, head);
	 
	 vector<int> scores;
	 int max;
	 int index_of_max;
	 for(unsigned int i = 0; i < head->adjBoards.size(); i++)
	 {
		 scores.push_back(alphaBeta(head->adjBoards[i], DEPTH, std::numeric_limits<int>::min(), std::numeric_limits<int>::max(), true));
		 if(i == 0)
		 {
			 max = scores[0];
			 index_of_max = 0;
		 }
		 else
		 {
			 if(scores[i] > max) 
			 {
				 max = scores[i];
				 index_of_max = i;
			 }

		 }
	 
	 }

     // Analyze score from board tree to decide which move is best
     vector <Move*> candidate_moves = get_possible_moves(head->myBoard, color);
     

		      for(unsigned int b = 0; b < scores.size(); b++)
		      {
		          fprintf(stderr, "\nThis is the score for move: (%i, %i): %i",candidate_moves[b]->getX(), candidate_moves[b]->getY(), scores[b]);
		      }


	    board->doMove(candidate_moves[index_of_max], color);
	 
		//fprintf(stderr, "\nThis is the move we are doing next: %d,%d\n",candidate_moves[index_of_correct_move]->getX(),candidate_moves[index_of_correct_move]->getY());
//fprintf(stderr, "\nThis is the index of best move: %i\n",index_of_correct_move);
	fprintf(stderr, "\n(%i, %i) is the best move with a score of: %i\n",candidate_moves[index_of_max]->getX(), candidate_moves[index_of_max]->getY(), max);	
		return candidate_moves[index_of_max];
	}
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    Algorithm Tests
        
        3/16 at 9:30pm Opening: Strategies, Mid: Position Value Table, End: Brute Force
        * W|L Ratio: 4:10   Null: 1
    
        edit 9:48pm: in opening, dont favor corners.
        * W|L Ratio: 2:6   Null: 1
      
        edit 10:02pm: depth set to 1
        * W|L Ratio: 0:7
        fuk dat
        
        
    
    
    
    */


